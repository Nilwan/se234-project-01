package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import model.Ball;
import model.Box;
import model.Character;
import model.Keys;
import model.enemy;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 864;
    public static final int HEIGHT = 608;
    public static final int GROUND = 550;
    public static final int CENTER = WIDTH/2;

    private Image platformImg;

    private ArrayList<Character> characterList;
    private ArrayList<enemy> enemyList;
    private Ball ball;
    private Ball ball2;
    private ArrayList<Box> boxList; // new1
    private static ArrayList<Score> scoreList;
    private enemy e = new enemy(5,10);

    private Keys keys;

    public Platform() {

        characterList = new ArrayList<>();

        enemyList = new ArrayList<>();

        boxList = new ArrayList<>(); // new1


        scoreList = new ArrayList();
        scoreList.add(new Score(30,30));
        scoreList.add(new Score(Platform.WIDTH-80,30));

        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/assets/bg.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);

        characterList.add(new Character(30, Platform.GROUND - 128, 2, 270, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.SPACE, KeyCode.Q,KeyCode.E,
                "/assets/sprite_sheet.png", 128, 128, 6,6,1,65,64, 0,1,2,1, 2, 2, "left"));
        characterList.add(new Character(Platform.WIDTH - 160, Platform.GROUND - 150, 0, 0, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.UP, KeyCode.ENTER, KeyCode.Q,KeyCode.M,
                "/assets/char06.png",150, 170, 4,4,1,70,55,0,1,1,1,0,2, "right"));
        ball = new Ball(40,30, 87, 155,"/assets/sprite_sheet.png", 1,1,1,43,45);
        ball2 = new Ball(-900000,-100000, 87, 155,"/assets/sprite_sheet.png", 1,1,1,43,45);
        // new1
        boxList.add(new Box(200,-200, 0, 0,"/assets/box.png", 1,1,1,70,70,1,2));
        boxList.add(new Box(Platform.WIDTH-300,-200, 0, 0,"/assets/box.png", 1,1,1,70,70,1,2));

        //add enemies
        enemyList.add(characterList.get(0).getFlame());
        enemyList.add(characterList.get(1).getFlame());

        getChildren().add(backgroundImg);
        getChildren().addAll(characterList);
        getChildren().addAll(enemyList); //enemy
        getChildren().addAll(ball);
        getChildren().addAll(ball2);
        getChildren().addAll(scoreList);
        getChildren().addAll(boxList); // new1
    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public Ball getBall() {
        return ball;
    }

    public Ball getBall2() {
        return ball2;
    }

    public Keys getKeys() {
        return keys;
    }

    public static ArrayList<Score> getScoreList() {
        return scoreList;
    }


    public enemy getEnemy(int i) {
        return characterList.get(i).getFlame();
    }


    public ArrayList<Box> getBoxList() { 
        return boxList;

    }
}

