package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class enemy extends Pane {
    private int x;
    private int y;
    public int CHARACTER_WIDTH = 50;
    public int CHARACTER_HEIGHT = 62;

    private Image img;
    private ImageView imageView;

    private int startX;
    private int startY;
    private boolean appearing;

    public enemy(int x,int y){
        this.startX = x;
        this.startY = y;
        this.x = -100;
        this.y = -100;

        this.img = new Image(getClass().getResourceAsStream("/assets/Flame.png"));
        this.imageView = new ImageView(img);

        appearing = false;

        setTranslateX(this.x);
        setTranslateY(this.y);
        this.getChildren().addAll(this.imageView);
    }

    public  void isCalled(){
        x = startX;
        y = startY;
        repaint();
        appearing = true;
    }

    public void hide(){
        x = -100;
        y = -100;
        repaint();
        appearing = false;
    }

    public boolean isOnscreen(){
        return appearing;
    }

    public void repaint(){
        setTranslateX(x);
        setTranslateY(y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
