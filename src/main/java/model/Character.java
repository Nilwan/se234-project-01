package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.Platform;

public class Character extends Pane {

    Logger logger = LoggerFactory.getLogger(Character.class);

    public int CHARACTER_WIDTH = 0;
    public int CHARACTER_HEIGHT = 0;

    private Image characterImg;
    private Image specialEffect;
    private AnimatedSprite imageView;

    private String side;

    private int x;
    private int y;

    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;

    private int score = 0;

    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    private KeyCode diveKey;
    private KeyCode showball;
    private KeyCode special;

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 7;
    int yMaxVelocity = 22; // สูง
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean isDiving = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    boolean buffing = false;


    private enemy flame;

    

    ///////////
    int random = 0;
    ///////////

    public Character(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode upKey, KeyCode diveKey,KeyCode showball,KeyCode special,String name, int CHARACTER_HEIGHT, int CHARACTER_WIDTH, int count, int columns, int rows, int width, int height,
                     int jumpXIndex, int jumpYIndex, int fallXIndex, int fallYIndex, int diveXIndex, int diveYIndex, String side) {

        this.startX = x;
        this.startY = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;

        this.CHARACTER_HEIGHT = CHARACTER_HEIGHT;
        this.CHARACTER_WIDTH = CHARACTER_WIDTH;

        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.characterImg = new Image(getClass().getResourceAsStream(name));
        this.imageView = new AnimatedSprite(characterImg, count, columns, rows, offsetX, offsetY, width, height, jumpXIndex, jumpYIndex, fallXIndex, fallYIndex, diveXIndex, diveYIndex);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.diveKey = diveKey;
        this.special = special;
        this.side = side;
        this.showball=showball;
        if (side.equals("right")) {
            this.setScaleX(-1);
        }
        this.getChildren().addAll(this.imageView);

        if(side.equals("right")){
            this.flame = new enemy(200, y+30);
        }else {
            this.flame = new enemy(580, y+30);
        }


    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }

    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            yVelocity = yMaxVelocity;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }

    public void checkReachHighest() {
        if (jumping && yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachFloor() {
        if (falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
            falling = false;
            canJump = true;
            yVelocity = 0;
        }
    }

    public void checkReachGameWall() {
        if (x <= 0) {
            x = 0;
        } else if (x + getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH - CHARACTER_WIDTH;
        }
    }

    //new5552
    public void checkReachGameCenter() {
        if (getSide().equals("left")) {
            if (x + getWidth() >= Platform.CENTER) {
                x = Platform.CENTER - CHARACTER_WIDTH;
            }
        } else {
            if (x <= Platform.CENTER + 10) {
                x = Platform.CENTER + 10;
            }
        }
    }

    public void moveX() {
        setTranslateX(x);

        if (isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + xAcceleration;
            x = x - xVelocity;
        }
        if (isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + xAcceleration;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if (falling) {
            yVelocity = yVelocity >= yMaxVelocity ? yMaxVelocity : yVelocity + yAcceleration;
            y = y + yVelocity;
        } else if (jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity - yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    public void trace(String action) {
        logger.info("x:{} y:{} vx:{} vy:{} action:{} Score Left:{} Score Right:{}", x, y, xVelocity, yVelocity, action, Platform.getScoreList().get(0).getScore(), Platform.getScoreList().get(1).getScore());
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public KeyCode getDiveKey() {
        return diveKey;
    }

    public AnimatedSprite getImageView() {
        return imageView;
    }

    public int getScore() {
        return score;
    }

    public KeyCode getshowball() { return showball; }

    public void setScore(int score) {
        this.score = score;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public enemy getFlame(){
        return flame;
    }

    public boolean isMoveLeft() {
        return isMoveLeft;
    }

    public boolean isMoveRight() {
        return isMoveRight;
    }

    public boolean isFalling() {
        return falling;
    }

    public boolean isCanJump() {
        return canJump;
    }

    public boolean isJumping() {
        return jumping;
    }

    public boolean isBuffing() {
        return buffing;
    }

    public String getSide() {
        return side;
    }

    public void setDiving(boolean diving) {
        isDiving = diving;
    }


    ///////

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }

    ///////

    public void speedUpWhenGetEvery3Point() {

        new Thread(() -> {
            long speedNow = System.currentTimeMillis();
            long after3Sec = speedNow + 3000;
            while (after3Sec > System.currentTimeMillis()) {
                xMaxVelocity = 30;
            }
            xMaxVelocity = 7;
        }).start();
    }

    public void speedDownWhenEnemyGetEvery3Point() {

         new Thread(() -> {
            long speedNow = System.currentTimeMillis();
            long after3Sec = speedNow + 3000;
            while (after3Sec > System.currentTimeMillis()) {
                xMaxVelocity = 2;
            }
            xMaxVelocity = 7;
        }).start();
    }

    public void getSpecialbuff() {
        System.out.println("buff start");
        new Thread(() -> {
            long speedNow = System.currentTimeMillis();
            long after3Sec = speedNow + 3000;
            long coolDown = speedNow + 8000;
            while (after3Sec > System.currentTimeMillis()) {
                    buffing = true;
            }
            while (coolDown > System.currentTimeMillis()) {

            }
            buffing = false;
            System.out.println("CD end");
        }).start();
    }

    public void callEnemy() {
        System.out.println("call enemy");
        System.out.println(x+"  f: "+ flame.getX());
        new Thread(() -> {
            long speedNow = System.currentTimeMillis();
            long after3Sec = speedNow + 3000;
            while (after3Sec > System.currentTimeMillis()) {
                flame.isCalled();
            }
            flame.hide();
            System.out.println("call enemy end");
        }).start();
    }


    public void colliedTheEnemy(enemy c) throws InterruptedException {
        if (c.isOnscreen()){
            if (!jumping && !falling && isMoveLeft && x <= c.getX() + c.CHARACTER_WIDTH && x+CHARACTER_WIDTH >= c.getX() ) {
                x = c.getX() + c.CHARACTER_WIDTH ;
                stop();
            } else if (!jumping && !falling && isMoveRight && x <= c.getX() + c.CHARACTER_WIDTH && x+CHARACTER_WIDTH >= c.getX()  ) {
                x = c.getX()- CHARACTER_WIDTH ;
                stop();
            }
        }

//        if(y < Platform.GROUND - 62) {
//            if( falling && y < c.getY() && Math.abs(y-c.getY())<=CHARACTER_HEIGHT+1) {
//
//            }
//        }
    }

    public KeyCode getSpecialKey() {
        return special;
    }
}
