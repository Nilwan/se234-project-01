package model;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AnimatedSprite extends ImageView{

    int count, columns, rows, offsetX, offsetY, width, height, curXIndex=0, curYIndex=0,
            jumpXIndex=0, jumpYIndex=0, fallXIndex=0, fallYIndex=0, diveXIndex, diveYIndex;

    public AnimatedSprite(Image image, int count, int columns, int rows, int offsetX, int offsetY, int width, int height,
                          int jumpXIndex, int jumpYIndex, int fallXIndex, int fallYIndex, int diveXIndex, int diveYIndex){
        this.setImage(image);
        this.count = count;
        this.columns = columns;
        this.rows = rows;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.jumpXIndex = jumpXIndex;
        this.jumpYIndex = jumpYIndex;
        this.fallXIndex = fallXIndex;
        this.fallYIndex = fallYIndex;
        this.diveXIndex = diveXIndex;
        this.diveYIndex = diveYIndex;
        this.setViewport(new Rectangle2D(offsetX, offsetY, width, height));
    }

    public void tick() {
        curXIndex = (curXIndex+1)%columns;
        curYIndex = (curYIndex+1)/columns;
        interpolate();
    }

    public void tickJump() {
        curXIndex = jumpXIndex;
        curYIndex = jumpYIndex;
        interpolate();
    }

    public void tickFall() {
        curXIndex = fallXIndex;
        curYIndex = fallYIndex;
        interpolate();
    }

    public void tickDive() {
        curXIndex = diveXIndex;
        curYIndex = diveYIndex;
        interpolate();
    }

    protected void interpolate() {
        final int x = curXIndex*width+offsetX;
        final int y = curYIndex*height+offsetY;
        this.setViewport(new Rectangle2D(x, y, width, height));
    }

    public void resetIndex() {
        curXIndex = 0;
        curYIndex = 0;
        interpolate();
    }
}
