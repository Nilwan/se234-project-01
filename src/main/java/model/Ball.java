package model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import view.Platform;

import java.util.ArrayList;

public class Ball extends Pane {

    public static final int BALL_WIDTH = 80;
    public static final int BALL_HEIGHT = 80;

    private String side = "left";

    private Image ballImg;
    private AnimatedBall ballView;

    private int x;
    private int y;
    private int offsetX;
    private int offsetY;
    private int xVelocity = 0;
    private int yVelocity = 0;
    int xMaxVelocity = 10;
    int yMaxVelocity = 25;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;

    boolean falling = true;
    boolean flowing = false;

    public Ball(int x, int y, int offsetX, int offsetY, String name, int count,
                int columns, int rows, int width, int height) {
        this.x = x;
        this.y = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;

        this.ballImg = new Image(getClass().getResourceAsStream(name));
        this.ballView = new AnimatedBall(ballImg, count, columns, rows, offsetX, offsetY, width, height);
        this.ballView.setFitWidth(BALL_WIDTH);
        this.ballView.setFitHeight(BALL_HEIGHT);

        this.getChildren().addAll(this.ballView);
    }

    public void checkBallReachFloor(ArrayList<Character> characterList) {
        if (falling && y >= Platform.GROUND - BALL_HEIGHT) {
            falling = false;
            yVelocity = 0;
            if (x > Platform.CENTER) {
                characterList.get(0).setScore(characterList.get(0).getScore() + 1);
                characterList.get(0).trace("Left Get Score");
                side = "left";
//                if (characterList.get(0).getScore() % 3 == 0) {
//                    speed();
//                    characterList.get(0).speedUpWhenGetEvery3Point();
//                    characterList.get(1).speedDownWhenEnemyGetEvery3Point();
//                }
//                if (characterList.get(0).getScore() % 5 == 0) {
//                    slow();
//                }
            } else {
                characterList.get(1).setScore(characterList.get(1).getScore() + 1);
                characterList.get(1).trace("Right Get Score");
                side = "right";
//                if (characterList.get(1).getScore() % 3 == 0) {
//                    speed();
//                    characterList.get(1).speedUpWhenGetEvery3Point();
//                    characterList.get(0).speedDownWhenEnemyGetEvery3Point();
//                }
//                if (characterList.get(1).getScore() % 5 == 0) {
//                    slow();
//                }
            }
            respawn();
        }
    }

    public void checkBallReachHighest() {
        if (flowing && yVelocity <= 0) {
            flowing = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkBallReachGameWall() {
        if (x <= 0) {
            isMoveLeft = false;
            isMoveRight = true;
        } else if (x + getWidth() >= Platform.WIDTH) {
            isMoveLeft = true;
            isMoveRight = false;
        }
    }

    public void respawn() {
        if (side.equals("left")) {
            x = 40;
            y = 30;
        } else {
            x = Platform.WIDTH - BALL_WIDTH - 30;
            y = 30;
        }
        isMoveLeft = false;
        isMoveRight = false;
        falling = true;
        flowing = false;
    }

    public void moveBallX() {
        setTranslateX(x);

        if (isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + 1;
            x = x - xVelocity;
        }
        if (isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + 1;
            x = x + xVelocity;
        }
    }

    public void moveBallY() {
        setTranslateY(y);

        if (falling) {
            yVelocity = yVelocity >= yMaxVelocity ? yMaxVelocity : yVelocity + 1;
            y = y + yVelocity;
        } else if (flowing) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity - 1;
            y = y - yVelocity;
        }
    }

    public void repaintBall() {
        moveBallX();
        moveBallY();
    }


    public void collidedCharacter(Character c) {
        int b = 0;
        if(c.isBuffing()){
            b = 50;
        }
        if (x >= c.getX()-50-b && x <= c.getX()+c.CHARACTER_WIDTH+50+b && y >= c.getY()-c.CHARACTER_HEIGHT-20){

            if (!c.isMoveLeft && !c.isMoveRight) {
                x = c.getX();
                yVelocity = yMaxVelocity;
                falling = false;
                flowing = true;
            } else if (c.isMoveRight && c.canJump && falling) {
                x = x + xMaxVelocity;
                y = c.getY() - 20;
                yVelocity = yMaxVelocity;
                isMoveRight = true;
                isMoveLeft = false;
                falling = false;
                flowing = true;
            } else if (c.isMoveRight && !c.canJump && falling) {
                x = x + xMaxVelocity;
                y = c.getY() - 20;
                isMoveRight = true;
                isMoveLeft = false;
            } else if (c.isMoveLeft && c.canJump && falling) {
                x = x - xMaxVelocity;
                y = c.getY() - 20;
                yVelocity = yMaxVelocity;
                isMoveLeft = true;
                isMoveRight = false;
                falling = false;
                flowing = true;
            } else if (c.isMoveLeft && !c.canJump && falling) {
                x = x - xMaxVelocity;
                y = c.getY() - 20;
                yVelocity = yMaxVelocity;
                isMoveLeft = true;
                isMoveRight = false;
            }
        }


    }

    public void speed() {
        new Thread(() -> {
            long now = System.currentTimeMillis();
            long Three = now + 3000;
            while (System.currentTimeMillis() < Three) {
                yMaxVelocity = 30;
            }
            yMaxVelocity = 15;
        }).start();
    }

    public void slow() {
        new Thread(() -> {
            long now = System.currentTimeMillis();
            long Three = now + 3000;
            while (System.currentTimeMillis() < Three) {
                yMaxVelocity = 5;
            }
            yMaxVelocity = 30;
        }).start();
    }

    public void showballkey(){

        x=40;
        y=30;
        new Thread(()->{
            long now = System.currentTimeMillis();
            long afterFive = now +5000;
            while(System.currentTimeMillis()<afterFive){

            }x=-60000;  y=-60000;

        }).start();
    }
}

