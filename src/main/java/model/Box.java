package model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Box extends Pane {

    public static final int BOX_WIDTH = 90;
    public static final int BOX_HEIGHT = 90;

    private Image boxImg;
    private AnimatedBox boxView;

    private int x;
    private int y;
    private int offsetX;
    private int offsetY;
    private boolean isDisplay;

    public Box(int x, int y, int offsetX, int offsetY, String name, int count,
                int columns, int rows, int width, int height, int redXIndex, int greenXIndex) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.offsetX = offsetX;
        this.offsetY = offsetY;

        this.boxImg = new Image(getClass().getResourceAsStream(name));
        this.boxView = new AnimatedBox(boxImg, count, columns, rows, offsetX, offsetY, width, height, redXIndex, greenXIndex);
        this.boxView.setFitWidth(BOX_WIDTH);
        this.boxView.setFitHeight(BOX_HEIGHT);

        this.isDisplay = false;

        this.getChildren().addAll(this.boxView);
    }

    public void repaint() {
        if(isDisplay){
            this.y = -200;
            this.isDisplay = false;
        }else {
            this.y = 100;
            this.isDisplay = true;
        }
        this.setTranslateY(this.y);
    }

    public boolean isDisplay() {
        return isDisplay;
    }

    public AnimatedBox getBoxView() {
        return boxView;
    }
}
