package model;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AnimatedBox extends ImageView {

    int count, columns, rows, offsetX, offsetY, width, height, redXIndex, greenXIndex, curXIndex=0, curYIndex=0;

    public AnimatedBox(Image image, int count, int columns, int rows, int offsetX, int offsetY, int width, int height, int redXIndex, int greenXIndex){
        this.setImage(image);
        this.count = count;
        this.columns = columns;
        this.rows = rows;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.redXIndex = redXIndex;
        this.greenXIndex = greenXIndex;
        this.setViewport(new Rectangle2D(offsetX, offsetY, width, height));
    }
    public void tick() {
        curXIndex = 0;
        interpolate();
    }

    public void tickRedBox() {
        curXIndex = redXIndex;
        interpolate();
    }

    public void tickGreenBox() {
        curXIndex = greenXIndex;
        interpolate();
    }

    protected void interpolate() {
        final int x = curXIndex*width+offsetX;
        final int y = curYIndex*height+offsetY;
        this.setViewport(new Rectangle2D(x, y, width, height));
    }
}
