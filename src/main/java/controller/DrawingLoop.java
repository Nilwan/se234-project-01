package controller;

import model.Ball;
import model.Box;
import model.Character;
import model.enemy;
import view.Platform;

import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    private int mibmubBox;
    private int ccolorBox;
    private int mubmibBox;
    private int counter;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 45;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        mibmubBox = 600;
        ccolorBox = 50;
        mubmibBox = 100;
        counter = 0;

        running = true;
    }

    private void checkDrawCollisions(ArrayList<Character> characterList) {
        for (Character character : characterList) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            character.checkReachGameCenter();//new5551

        }
    }

    private void checkBallCollisions(ArrayList<Character> characterList, Ball ball, Ball ball2) {
        ball.checkBallReachFloor(characterList);
        ball.checkBallReachHighest();
        ball.checkBallReachGameWall();

        ball2.checkBallReachFloor(characterList);
        ball2.checkBallReachHighest();
        ball2.checkBallReachGameWall();
    }

    private void checkBallCollisionsCharacter(ArrayList<Character> characterList, Ball ball,Ball ball2) {
        for (Character c : characterList) {
            if (c.getBoundsInParent().intersects(ball.getBoundsInParent())) {
                ball.collidedCharacter(c);
                return;
            }
        }

        for (Character c : characterList) {
            if (c.getBoundsInParent().intersects(ball.getBoundsInParent())) {
                ball2.collidedCharacter(c);
                return;
            }
        }
    }
private void checkCollisionsEnemy(ArrayList<Character> characterList) throws InterruptedException {
        characterList.get(0).colliedTheEnemy(characterList.get(1).getFlame());
        characterList.get(1).colliedTheEnemy(characterList.get(0).getFlame());
    }

    private void paint(ArrayList<Character> characterList, Ball ball,Ball ball2) {
        for (Character character : characterList) {
            character.repaint();
            character.getFlame().repaint();
        }
        ball.repaintBall();
        ball2.repaintBall();
    }

    private void displayBox(ArrayList<Box> boxArrayList){
        for (Box box : boxArrayList) {
            box.getBoxView().tick();
            box.repaint();
        }
    }

    private void randomBox(ArrayList<Box> boxArrayList, ArrayList<Character> characterList){
        characterList.get(0).setRandom(0);
        characterList.get(1).setRandom(0);
        for(int i=0; i<boxArrayList.size(); i++){
            int random = (int)(Math.random()*2);
            if(random == 0){
                boxArrayList.get(i).getBoxView().tickRedBox();
                //rand only debuff to char
                do {
                    if(i==1 && (characterList.get(0).getRandom() == 3 || characterList.get(0).getRandom() == 4)){
//                        System.out.println("hit red");
                        do{
                            random = (int)((Math.random()*4)+1);
                        }while (random == 4);
                    }else {
                        random = (int)((Math.random()*4)+1);
                    }
                }while (random%2!=0); //only even allow
                characterList.get(i).setRandom(random);
            }else {
                boxArrayList.get(i).getBoxView().tickGreenBox();
                //rand only buff to char
                do {
                    if(i==1 && (characterList.get(0).getRandom() == 3 || characterList.get(0).getRandom() == 4)){
//                        System.out.println("hit green");
                        do{
                            random = (int)((Math.random()*4)+1);
                        }while (random == 3);
                    }else {
                        random = (int) ((Math.random() * 4) + 1);
                    }
                }while (random%2!=1); //only odd allow
                characterList.get(i).setRandom(random);
            }
        }
        if(characterList.get(0).getRandom() == 1){
            characterList.get(0).speedUpWhenGetEvery3Point();
        }
        if(characterList.get(1).getRandom() == 1){
            characterList.get(1).speedUpWhenGetEvery3Point();
        }
        if(characterList.get(0).getRandom() == 2){
            characterList.get(1).speedDownWhenEnemyGetEvery3Point();
        }
        if(characterList.get(1).getRandom() == 2){
            characterList.get(0).speedDownWhenEnemyGetEvery3Point();
        }
        if(characterList.get(0).getRandom() == 3 || characterList.get(1).getRandom() == 3){
            platform.getBall().speed();
        }
        if(characterList.get(0).getRandom() == 4 || characterList.get(1).getRandom() == 4){
            platform.getBall().slow();
        }
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            checkDrawCollisions(platform.getCharacterList());
            checkBallCollisions(platform.getCharacterList(), platform.getBall(), platform.getBall2());
            checkBallCollisionsCharacter(platform.getCharacterList(), platform.getBall(), platform.getBall2());
            try {
                checkCollisionsEnemy(platform.getCharacterList());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            paint(platform.getCharacterList(), platform.getBall(), platform.getBall2());

            counter++;
            if(counter == mibmubBox && !platform.getBoxList().get(0).isDisplay()){
                counter = 0;
                displayBox(platform.getBoxList());
            }else if(counter == ccolorBox && platform.getBoxList().get(0).isDisplay()){
                randomBox(platform.getBoxList(), platform.getCharacterList());
            }else if(counter == (ccolorBox+mubmibBox) && platform.getBoxList().get(0).isDisplay()){
                counter = 0;
                displayBox(platform.getBoxList());
            }
            else {
                //System.out.println(counter);
            }


            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
