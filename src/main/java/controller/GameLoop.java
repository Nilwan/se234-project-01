package controller;

import model.Character;
import view.Platform;
import view.Score;

import java.security.Key;
import java.util.ArrayList;

public class GameLoop<ball> implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private Key key;
    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 10;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void update(ArrayList<Character> characterList) {


        for (Character character : characterList) {
            if (platform.getKeys().isPressed(character.getshowball())){
                platform.getBall2().showballkey();
            }
            }

        for (Character character : characterList) {
            if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey()) && !platform.getKeys().isPressed(character.getDiveKey())) {
                character.getImageView().tick();
            }

            //move left
            if (platform.getKeys().isPressed(character.getLeftKey())) {
                character.setScaleX(-1);
                character.moveLeft();
                character.trace("Move Left");
            }

            //move right
            if (platform.getKeys().isPressed(character.getRightKey())) {
                character.setScaleX(1);
                character.moveRight();
                character.trace("Move Right");
            }

            //x l no r stop 555
            if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())) {
                character.stop();
            }

            //jump
            if (platform.getKeys().isPressed(character.getUpKey())) {
                character.getImageView().tickJump();
                character.jump();
                character.trace("Jump");
            }

            //falling +
            if(character.isFalling()){
                character.getImageView().tickFall();
            }

            //on ground ac index
            if(character.isCanJump() && !platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())){
                character.getImageView().resetIndex();
                character.setDiving(false);
            }

            //dive left
            if (platform.getKeys().isPressed(character.getDiveKey()) && platform.getKeys().isPressed(character.getLeftKey())) {
                character.setScaleX(-1);
                character.moveLeft();
                character.getImageView().tickDive();
                character.setDiving(true);
                character.trace("Dive Left");
            }

            //dive right
            if (platform.getKeys().isPressed(character.getDiveKey()) && platform.getKeys().isPressed(character.getRightKey())) {
                character.setScaleX(1);
                character.moveRight();
                character.getImageView().tickDive();
                character.setDiving(true);
                character.trace("Dive Right");
            }

            //special attack
            if (platform.getKeys().isPressed(character.getSpecialKey())) {
                if(!character.isBuffing()) {
                    character.getSpecialbuff();
                    character.trace("special is starting");
                }
            }

            //call enemy
            if (platform.getKeys().isPressed(character.getSpecialKey()) && platform.getKeys().isPressed(character.getUpKey())) {
                if(!character.getFlame().isOnscreen()) {
                    character.callEnemy();
                    character.trace("call a summon");
                }
            }
        }
    }

    private void updateScore(ArrayList<Score> scoreList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i = 0; i < scoreList.size(); i++) {
                scoreList.get(i).setPoint(characterList.get(i).getScore());
            }
        });
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getCharacterList());
            updateScore(platform.getScoreList(),platform.getCharacterList()); //5.4.3

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
